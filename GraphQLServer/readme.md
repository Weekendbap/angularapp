Firstly, download related packages: mvn clean install
Run graphql server (spring boot): mvn spring-boot:run

    How to query?
	1 - Using GraphiQL tool to request
	Single endpoint: http://localhost:8080/graphql
	With header: Authorization: token <access token>

	E.g: access token: d41784e577d8ed4f8dc79c3bba2578f81d6ed4cd

	Ex body query: {
	  viewer {
	    location
	    email
	    avatar_url
	    blog
	    company
	    repos(repoName: "test") {
	      name
	      created_at
	      issues(state: "all") {
		number
		title
		body
		state
		created_at
		closed_at
		labels
	      }
	    }
	  }
	}

	Note: state => Indicates the state of the issues to return. Can be either open, closed, or all. Default: open
	      repos => Indicates the name of the repo to return. If empty, return all repos.

	2 - Using Postman tool to request (ALWAYS USE POST METHOD)
	endpoint & header as above

	Ex1 body query: {
		"query": "{viewer {location, email}}"
	}

	Ex2 body mutation (for create, update): {
		"query": "mutation {createIssue(repoName: \"test\", title: \"d\", body: \"d\", labels: [\"bug\", \"new feature\"]) {number,title}}"
	}

-----------------------------------------------------------------------------
	More API
	1. Create a issue (with access token)
	
	Note: repoName, title are required field
	Syntax: mutation {
		  createIssue(repoName: "test", title: "d", body: "d", labels: ["bug", "new feature"]) {
		    number
		    title
		    body
		    state
		    created_at
		    closed_at
		    labels
		  }
		}

	2. Patch a issue (with access token)

	Note: number, repoName are required field
	Syntax: mutation {
		  createIssue(number: 1, repoName: "test", title: "d", body: "d", labels: ["bug", "new feature"]) {
		    number
		    title
		    body
		    state
		    created_at
		    closed_at
		    labels
		  }
		}
	3. Count labels (with access token)

	Syntax: {
		  viewer {
		    repos {
		      name
		      created_at
		      labelStatistic{
			bug
			duplicate
			enhancement
			goodFirstIssue
			helpWanted
			invalid
			newFeature
			question
			wontfix
		      }
		    }
		  }
		} 

Note: Get "access token" from GitHub
1. Users are redirected to request their GitHub identity: https://github.com/login/oauth/authorize?client_id=60915e68fa06a37e81f8&scope=user%20notifications%20repo 
=> get the code in url link after authorization 
2. Get access token by post request: https://github.com/login/oauth/access_token?client_id=60915e68fa06a37e81f8&client_secret=8d818fd3b263ca550df66dbedee93dc2ee0ec795&code=<your code>




