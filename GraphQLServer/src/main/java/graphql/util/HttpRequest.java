package graphql.util;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.ParseException;

import java.lang.*;

/**
 * Http request util
 */
public class HttpRequest {
    private static Integer TIMEOUT = 50000;

    /**
     * Get request with token and params
     * @param getUrl
     * @param token
     * @param params
     * @return
     */
    static public String getWithTokenAndParams( String getUrl, String token, Map<String, String> params ) {
	try {
	    if ( params.size() > 0 ) {
		getUrl = getUrl.concat( "?" );
		for ( Map.Entry<String, String> entry: params.entrySet() ) {
		    getUrl = getUrl.concat( entry.getKey() ).concat( "=" ).concat( entry.getValue() ).concat( "&" );
		}
		getUrl = getUrl.substring( 0, getUrl.length() - 1 );
	    }

	    URL url = new URL( getUrl );
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    conn.setRequestMethod( "GET" );
	    conn.setRequestProperty( "Accept", "application/json" );
	    conn.setRequestProperty( "Authorization", token );
	    conn.setConnectTimeout( TIMEOUT );
	    conn.setReadTimeout( TIMEOUT );
	    return read( conn );
	} catch ( IOException ioe ) {
	    ioe.printStackTrace();
	}

	return null;
    }

    /**
     * Get request with token
     *
     * @param getUrl
     * @param token
     * @return
     */
    static public String getWithToken( String getUrl, String token ) {
	try {
	    URL url = new URL( getUrl );
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    conn.setRequestMethod( "GET" );
	    conn.setRequestProperty( "Accept", "application/json" );
	    conn.setRequestProperty( "Authorization", token );
	    conn.setConnectTimeout( TIMEOUT );
	    conn.setReadTimeout( TIMEOUT );

	    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String format = formatter.format(conn.getDate());
	    System.out.println("Start time GraphQL to GITHUB: " +format );
	    return read( conn );
	} catch ( IOException ioe ) {
	    ioe.printStackTrace();
	}

	return null;
    }

    /**
     * Get request without token
     *
     * @param getUrl
     * @return
     */
    static public String getWithoutToken( String getUrl ) {
	try {
	    URL url = new URL( getUrl );
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    conn.setRequestMethod( "GET" );
	    conn.setRequestProperty( "Accept", "application/json" );
	    conn.setConnectTimeout( TIMEOUT );
	    conn.setReadTimeout( TIMEOUT );
	    return read( conn );
	} catch ( IOException ioe ) {
	    ioe.printStackTrace();
	}

	return null;
    }

    /**
     * Post request with token
     *
     * @param postUrl
     * @param data
     * @param token
     * @return
     */
    static public String postWithToken( String postUrl, String data, String token ) {
	try {
	    URL url = new URL( postUrl );
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    conn.setRequestMethod( "POST" );
	    conn.setRequestProperty( "Accept", "application/json" );
	    conn.setRequestProperty( "Content-Type", "application/json" );
	    conn.setRequestProperty( "Authorization", token );
	    conn.setConnectTimeout( TIMEOUT );
	    conn.setReadTimeout( TIMEOUT );
	    conn.setDoOutput( true );

	    sendData( conn, data );

	    return read( conn );

	} catch ( IOException ioe ) {
	    ioe.printStackTrace();
	}

	return null;
    }

    /**
     * Patch request with token
     *
     * @param postUrl
     * @param data
     * @param token
     * @return
     */
    static public String patchWithToken( String postUrl, String data, String token ) {
	try {
	    URL url = new URL( postUrl );
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    conn.setRequestMethod( "POST" );
	    conn.setRequestProperty("X-HTTP-Method-Override", "PATCH");
	    conn.setRequestProperty( "Accept", "application/json" );
	    conn.setRequestProperty( "Content-Type", "application/json" );
	    conn.setRequestProperty( "Authorization", token );
	    conn.setConnectTimeout( TIMEOUT );
	    conn.setReadTimeout( TIMEOUT );
	    conn.setDoOutput( true );
	    sendData( conn, data );

	    return read( conn );

	} catch ( IOException ioe ) {
	    ioe.printStackTrace();
	}

	return null;
    }

    /**
     * Send content of request
     *
     * @param con
     * @param data
     * @throws IOException
     */
    static private void sendData( HttpURLConnection con, String data ) throws IOException {
	DataOutputStream wr = null;
	try {
	    wr = new DataOutputStream( con.getOutputStream() );
	    wr.writeBytes( data );
	    wr.flush();
	    wr.close();
	} catch ( IOException exception ) {
	    throw exception;
	} finally {
	    closeQuietly( wr );
	}
    }

    /**
     * Read response from destination server
     *
     * @param conn
     * @return
     * @throws IOException
     */
    static private String read( HttpURLConnection conn ) throws IOException {
	// if ( conn.getResponseCode() < 200 || conn.getResponseCode() > 226 ) {
	//     throw new RuntimeException( "Failed : HTTP error code : "
	// 		    + conn.getResponseCode() );
	// }

	BufferedReader in = null;
	String inputLine;
	StringBuilder body;
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	try {
	    in = new BufferedReader( new InputStreamReader( conn.getInputStream() ) );

	    body = new StringBuilder();

	    while ( (inputLine = in.readLine()) != null ) {
			body.append( inputLine );

			String format = formatter.format(new Date().getTime());
		    System.out.println("Time BufferedReader: " +format);
	    }
	    in.close();

	    return body.toString();
	} catch ( IOException ioe ) {
	    throw ioe;
	} finally {
	    closeQuietly( in );
	}
    }

    /**
     * Close IO
     *
     * @param closeable
     */
    static private void closeQuietly( Closeable closeable ) {
	try {
	    if ( closeable != null ) {
		closeable.close();
	    }
	} catch ( IOException ex ) {

	}
    }
}
