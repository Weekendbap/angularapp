package graphql.repository;

import graphql.util.HttpRequest;

/**
 * User repository.
 * To connect to GitHub User Rest API
 */
public class UserRepository {
    private static final String endPoint = "https://api.github.com/user";

    /**
     * Request to get method user with token
     *
     * @param accessToken
     * @return
     */
    public String requestUserInfo( String accessToken ) {
	return HttpRequest.getWithToken( endPoint, accessToken );
    }
}
