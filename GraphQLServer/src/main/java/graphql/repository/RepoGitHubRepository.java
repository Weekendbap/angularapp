package graphql.repository;

import graphql.util.HttpRequest;

/**
 * Repository GitHub repository.
 * To connect to GitHub Repo Rest API
 */
public class RepoGitHubRepository {
    private static final String endPointWithLoginAndRepo = "https://api.github.com/repos/%s/%s";
    private static final String endPoint = "https://api.github.com/user/repos";

    /**
     * Request to get method repos with access token
     *
     * @param accessToken
     * @return
     */
    public String requestReposInfo( String accessToken ) {
        return HttpRequest.getWithToken( endPoint, accessToken );
    }

    /**
     * Request to get specific repo with access token
     * @param login
     * @param repoName
     * @param accessToken
     * @return
     */
    public String requestRepoInfo( String login, String repoName, String accessToken ) {
        return HttpRequest.getWithToken( String.format( endPointWithLoginAndRepo, login, repoName ), accessToken );
    }
}