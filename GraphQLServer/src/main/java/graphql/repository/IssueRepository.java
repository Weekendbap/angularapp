package graphql.repository;

import java.util.Map;

import graphql.util.HttpRequest;

/**
 * Issue repository.
 * To connect to GitHub Issue Rest API
 */
public class IssueRepository {
    private static final String endPoint = "https://api.github.com/repos/%s/%s/issues";

    /**
     * Request to get method issues
     *
     * @param username
     * @param repoName
     * @return
     */
    public String requestIssuesInfo( String username, String repoName, String accessToken, Map<String, String> params ) {
	return HttpRequest.getWithTokenAndParams( String.format( endPoint, username, repoName ), accessToken, params );
    }

    /**
     * Request to post method issue with access token
     *
     * @param username
     * @param repoName
     * @param data
     * @param accessToken
     * @return
     */
    public String requestCreateIssue( String username, String repoName, String data, String accessToken ) {
	return HttpRequest.postWithToken( String.format( endPoint, username, repoName ), data, accessToken );
    }

    /**
     * Request to patch method issue with access token
     * @param number
     * @param username
     * @param repoName
     * @param data
     * @param accessToken
     * @return
     */
    public String requestPatchIssue( Integer number, String username, String repoName, String data, String accessToken ) {
	return HttpRequest.patchWithToken( String.format( endPoint.concat( "/" ).concat( number.toString() ), username, repoName ), data, accessToken );
    }
}
