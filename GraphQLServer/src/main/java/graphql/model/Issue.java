package graphql.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * Issue model
 */
public class Issue {
    private Integer number;
    private String title;
    private String body;
    private String state;
    private List<String> labels;
    private String created_at;
    private String closed_at;

    /**
     * Constructor
     *
     * @param number
     * @param title
     * @param body
     * @param state
     * @param created_at
     */
    public Issue( Integer number, String title, String body, String state, String created_at ) {
	this.number = number;
	this.title = title;
	this.body = body;
	this.state = state;
	this.created_at = created_at;
    }

    /**
     * Constructor with json
     *
     * @param node
     */
    public Issue( JsonNode node ) {
	this.number = node.path( "number" ).asInt();
	this.title = node.path( "title" ).asText();
	this.body = node.path( "body" ).asText();
	this.state = node.path( "state" ).asText();
	this.created_at = node.path( "created_at" ).asText();
	this.closed_at = node.path( "closed_at" ).asText();

	JsonNode labelNodes = node.path( "labels" );

	List<String> labels = new ArrayList<>();
	for ( JsonNode labelNode : labelNodes ) {
	    String label = labelNode.path( "name" ).asText();

	    labels.add( label );
	}

	this.setLabels( labels );
	this.setClosed_at( closed_at );
    }

    /**
     * Getter/setter
     *
     * @return
     */
    public Integer getNumber() {
	return number;
    }

    public void setNumber( Integer number ) {
	this.number = number;
    }

    public String getTitle() {
	return title;
    }

    public void setTitle( String title ) {
	this.title = title;
    }

    public String getBody() {
	return body;
    }

    public void setBody( String body ) {
	this.body = body;
    }

    public String getState() {
	return state;
    }

    public void setState( String state ) {
	this.state = state;
    }

    public List<String> getLabels() {
	return labels;
    }

    public void setLabels( List<String> labels ) {
	this.labels = labels;
    }

    public String getCreated_at() {
	return created_at;
    }

    public void setCreated_at( String created_at ) {
	this.created_at = created_at;
    }

    public String getClosed_at() {
	return closed_at;
    }

    public void setClosed_at( String closed_at ) {
	this.closed_at = closed_at;
    }
}
