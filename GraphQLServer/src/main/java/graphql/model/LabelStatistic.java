package graphql.model;

/**
 * Label statistic
 * Counting bug, duplicate, etc.
 */
public class LabelStatistic {
    private Integer bug;
    private Integer duplicate;
    private Integer enhancement;
    private Integer goodFirstIssue;
    private Integer helpWanted;
    private Integer invalid;
    private Integer newFeature;
    private Integer question;
    private Integer wontfix;

    /**
     * Label enum
     */
    public enum Label {
	BUG( "bug" ), DUPLICATE( "duplicate" ), ENHANCEMENT( "enhancement" ), GOODFIRSTISSUE( "good first issue" ), HELPWANTED( "help wanted" ), INVALID( "invalid" ), NEWFEATURE( "new feature" ), QUESTION( "question" ), WONTFIX( "wontfix" );

	private String value;

	public String getValue() {
	    return value;
	}

	Label( String value ) {
	    this.value = value;
	}

	public Label hasLabel( String value ) {
	    for ( Label item : Label.values() ) {
		if ( value.equals( item.value ) ) {
		    return item;
		}
	    }
	    return null;
	}
    }

    //Constructor
    public LabelStatistic( Integer bug, Integer duplicate, Integer enhancement, Integer goodFirstIssue, Integer helpWanted, Integer invalid, Integer newFeature, Integer question, Integer wontfix ) {
	this.bug = bug;
	this.duplicate = duplicate;
	this.enhancement = enhancement;
	this.goodFirstIssue = goodFirstIssue;
	this.helpWanted = helpWanted;
	this.invalid = invalid;
	this.newFeature = newFeature;
	this.question = question;
	this.wontfix = wontfix;
    }

    //Getter/setter
    public Integer getBug() {
	return bug;
    }

    public void setBug( Integer bug ) {
	this.bug = bug;
    }

    public Integer getDuplicate() {
	return duplicate;
    }

    public void setDuplicate( Integer duplicate ) {
	this.duplicate = duplicate;
    }

    public Integer getEnhancement() {
	return enhancement;
    }

    public void setEnhancement( Integer enhancement ) {
	this.enhancement = enhancement;
    }

    public Integer getGoodFirstIssue() {
	return goodFirstIssue;
    }

    public void setGoodFirstIssue( Integer goodFirstIssue ) {
	this.goodFirstIssue = goodFirstIssue;
    }

    public Integer getHelpWanted() {
	return helpWanted;
    }

    public void setHelpWanted( Integer helpWanted ) {
	this.helpWanted = helpWanted;
    }

    public Integer getInvalid() {
	return invalid;
    }

    public void setInvalid( Integer invalid ) {
	this.invalid = invalid;
    }

    public Integer getNewFeature() {
	return newFeature;
    }

    public void setNewFeature( Integer newFeature ) {
	this.newFeature = newFeature;
    }

    public Integer getQuestion() {
	return question;
    }

    public void setQuestion( Integer question ) {
	this.question = question;
    }

    public Integer getWontfix() {
	return wontfix;
    }

    public void setWontfix( Integer wontfix ) {
	this.wontfix = wontfix;
    }
}
