package graphql.model;

import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * Repository GitHub model
 */
public class RepoGitHub {
    private String name;
    private String created_at;
    private List<Issue> issues;
    private User owner;
    private LabelStatistic labelStatistic;

    /**
     * Constructor
     */
    public RepoGitHub( String name, String created_at, User owner ) {
	this.name = name;
	this.created_at = created_at;
	this.owner = owner;
    }

    /**
     * Constructor with json
     *
     * @param node
     * @param owner
     */
    public RepoGitHub( JsonNode node, User owner ) {
	this.name = node.path( "name" ).asText();
	this.created_at = node.path( "created_at" ).asText();
	this.owner = owner;
    }

    /**
     * Getter/setter
     *
     * @return
     */
    public String getName() {
	return name;
    }

    public void setName( String name ) {
	this.name = name;
    }

    public String getCreate_at() {
	return created_at;
    }

    public void setCreate_at( String created_at ) {
	this.created_at = created_at;
    }

    public List<Issue> getIssues() {
	return issues;
    }

    public void setIssues( List<Issue> issues ) {
	this.issues = issues;
    }

    public String getCreated_at() {
	return created_at;
    }

    public void setCreated_at( String created_at ) {
	this.created_at = created_at;
    }

    public User getOwner() {
	return owner;
    }

    public void setOwner( User owner ) {
	this.owner = owner;
    }


    public LabelStatistic getLabelStatistic() {
	return labelStatistic;
    }

    public void setLabelStatistic( LabelStatistic labelStatistic ) {
	this.labelStatistic = labelStatistic;
    }
}
