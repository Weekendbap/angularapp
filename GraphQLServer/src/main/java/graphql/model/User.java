package graphql.model;

import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * User model
 */
public class User {
    private String location;
    private String email;
    private String avatar_url;
    private String blog;
    private String company;
    private String login;
    private List<RepoGitHub> repos;

    /**
     * Constructor
     *
     * @param location
     * @param email
     * @param avatar_url
     * @param blog
     * @param company
     * @param login
     */
    public User( String location, String email, String avatar_url, String blog, String company, String login ) {
	this.location = location;
	this.email = email;
	this.avatar_url = avatar_url;
	this.blog = blog;
	this.company = company;
	this.login = login;
    }

    /**
     * Constructor with json
     *
     * @param node
     */
    public User( JsonNode node ) {
	this.location = node.path( "location" ).asText();
	this.avatar_url = node.path( "avatar_url" ).asText();
	this.company = node.path( "company" ).asText();
	this.email = node.path( "email" ).asText();
	this.blog = node.path( "blog" ).asText();
	this.login = node.path( "login" ).asText();
    }

    /**
     * Getter/setter
     *
     * @return
     */
    public List<RepoGitHub> getRepos() {
	return repos;
    }

    public void setRepos( List<RepoGitHub> repos ) {
	this.repos = repos;
    }

    public String getLocation() {
	return location;
    }

    public void setLocation( String location ) {
	this.location = location;
    }

    public String getEmail() {
	return email;
    }

    public void setEmail( String email ) {
	this.email = email;
    }

    public String getAvatar_url() {
	return avatar_url;
    }

    public void setAvatar_url( String avatar_url ) {
	this.avatar_url = avatar_url;
    }

    public String getBlog() {
	return blog;
    }

    public void setBlog( String blog ) {
	this.blog = blog;
    }

    public String getCompany() {
	return company;
    }

    public void setCompany( String company ) {
	this.company = company;
    }

    public String getLogin() {
	return login;
    }

    public void setLogin( String login ) {
	this.login = login;
    }
}
