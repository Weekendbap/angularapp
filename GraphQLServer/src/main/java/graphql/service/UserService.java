package graphql.service;

import graphql.model.User;
import graphql.repository.UserRepository;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * User service
 */
public class UserService {
    public UserRepository userRepository;

    /**
     * Constructor
     */
    public UserService() {
	this.userRepository = new UserRepository();
    }

    /**
     * Get a user info
     *
     * @param accessToken
     * @return
     */
    public User getUserInfo( String accessToken ) {
	String response = userRepository.requestUserInfo( accessToken );
	try {
	    ObjectMapper mapper = new ObjectMapper();
	    JsonNode root = mapper.readTree( response );
	    return new User( root );
	} catch ( Exception ex ) {
	    ex.printStackTrace();
	}

	return null;
    }
}
