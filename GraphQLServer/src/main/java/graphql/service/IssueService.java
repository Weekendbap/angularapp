package graphql.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import graphql.model.Issue;
import graphql.model.LabelStatistic;
import graphql.repository.IssueRepository;
import graphql.repository.UserRepository;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Issue service
 */
public class IssueService {
    private static String OPEN = "open";
    private static String ALL = "all";
    private static String STATE = "state";

    public IssueRepository issueRepository;
    public UserRepository userRepository;

    /**
     * Constructor
     */
    public IssueService() {
	this.issueRepository = new IssueRepository();
	this.userRepository = new UserRepository();
    }

    /**
     * Get issues info
     *
     * @param username
     * @param repoName
     * @return
     */
    public List<Issue> getIssuesInfo( String username, String repoName, String state, String accessToken ) {
	// Check state
	Map<String, String> params =  new HashMap<>();
	if ( state != null && !state.equals( this.OPEN ) ) {
	    params.put( this.STATE, state );
	}
        String content = issueRepository.requestIssuesInfo( username, repoName, accessToken, params );
	try {
	    ObjectMapper mapper = new ObjectMapper();
	    JsonNode root = mapper.readTree( content );

	    List<Issue> issues = new ArrayList<>();
	    for ( JsonNode node : root ) {
		Issue issue = new Issue( node );
		issues.add( issue );
	    }
	    return issues;
	} catch ( Exception ex ) {
	    ex.printStackTrace();
	}

	return null;
    }

    /**
     * Create a issue
     *
     * @param nameRepo
     * @param title
     * @param body
     * @param labels
     * @param accessToken
     * @return
     */
    public Issue createIssue( String nameRepo, String title, String body, List<String> labels, String accessToken ) {
	String userResponse = userRepository.requestUserInfo( accessToken );
	String username = null;

	try {
	    ObjectMapper mapper = new ObjectMapper();
	    JsonNode root = mapper.readTree( userResponse );
	    // Get username
	    username = root.path( "login" ).asText();

	    Map<String, Object> jsMap = new HashMap<>();
	    jsMap.put( "title", title );

	    if ( body != null ) {
		jsMap.put( "body", body );
	    }
	    if ( labels != null ) {
		jsMap.put( "labels", labels );
	    }

	    String issueResponse = issueRepository.requestCreateIssue( username, nameRepo, new JSONObject( jsMap ).toString(), accessToken );
	    root = mapper.readTree( issueResponse );

	    return new Issue( root );
	} catch ( Exception ex ) {
	    ex.printStackTrace();
	}

	return null;
    }

    /**
     * Patch a issue
     *
     * @param number
     * @param nameRepo
     * @param title
     * @param body
     * @param labels
     * @param accessToken
     * @return
     */
    public Issue patchIssue( Integer number, String nameRepo, String title, String body, List<String> labels, String accessToken ) {
	String userResponse = userRepository.requestUserInfo( accessToken );
	String username = null;

	try {
	    ObjectMapper mapper = new ObjectMapper();
	    JsonNode root = mapper.readTree( userResponse );
	    // Get username
	    username = root.path( "login" ).asText();

	    Map<String, Object> jsMap = new HashMap<>();
	    jsMap.put( "title", title );

	    if ( title != null ) {
		jsMap.put( "title", title );
	    }
	    if ( body != null ) {
		jsMap.put( "body", body );
	    }
	    if ( labels != null ) {
		jsMap.put( "labels", labels );
	    }

	    String issueResponse = issueRepository.requestPatchIssue( number, username, nameRepo, new JSONObject( jsMap ).toString(), accessToken );
	    root = mapper.readTree( issueResponse );

	    return new Issue( root );
	} catch ( Exception ex ) {
	    ex.printStackTrace();
	}

	return null;
    }

    public LabelStatistic countLabelsForIssue( String username, String repoName, String accessToken ) {
	List<Issue> issues = this.getIssuesInfo( username, repoName, this.ALL, accessToken );

	Map<String, Integer> cntLabel = new HashMap<>();

	for ( LabelStatistic.Label item : LabelStatistic.Label.values() ) {
	    cntLabel.put( item.getValue(), 0 );
	    for ( Issue issue : issues ) {
		if ( issue.getLabels().contains( item.getValue() ) ) {
		    cntLabel.put( item.getValue(), cntLabel.get( item.getValue() ) + 1 );
		}
	    }
	}

	Integer bug = cntLabel.get( LabelStatistic.Label.BUG.getValue() );
	Integer duplicate = cntLabel.get( LabelStatistic.Label.DUPLICATE.getValue() );
	Integer enhancement = cntLabel.get( LabelStatistic.Label.ENHANCEMENT.getValue() );
	Integer goodFirstIssue = cntLabel.get( LabelStatistic.Label.GOODFIRSTISSUE.getValue() );
	Integer helpWanted = cntLabel.get( LabelStatistic.Label.HELPWANTED.getValue() );
	Integer invalid = cntLabel.get( LabelStatistic.Label.INVALID.getValue() );
	Integer newFeature = cntLabel.get( LabelStatistic.Label.NEWFEATURE.getValue() );
	Integer question = cntLabel.get( LabelStatistic.Label.QUESTION.getValue() );
	Integer wontfix = cntLabel.get( LabelStatistic.Label.WONTFIX.getValue() );

	return new LabelStatistic( bug, duplicate, enhancement, goodFirstIssue, helpWanted, invalid, newFeature, question, wontfix);
    }
}
