package graphql.service;

import java.util.ArrayList;
import java.util.List;

import graphql.model.RepoGitHub;
import graphql.model.User;
import graphql.repository.RepoGitHubRepository;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Repository GitHub service
 */
public class RepoGitHubService {
    public RepoGitHubRepository repoGitHubRepository;

    /**
     * Constructor
     */
    public RepoGitHubService() {
	this.repoGitHubRepository = new RepoGitHubRepository();
    }

    /**
     * Get repos Github info
     *
     * @param user
     * @param accessToken
     * @return
     */
    public List<RepoGitHub> getRepos( User user, String repoName, String accessToken ) {

        // If request a spectific repo
	if ( repoName == null ) {
	    String response = repoGitHubRepository.requestReposInfo( accessToken );

	    try {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode root = mapper.readTree( response );

		List<RepoGitHub> repoGitHubs = new ArrayList<>();
		for ( JsonNode node : root ) {
		    repoGitHubs.add( new RepoGitHub( node, user ) );
		}

		return repoGitHubs;
	    } catch ( Exception ex ) {
		ex.printStackTrace();
	    }
	}
        else {
	    String response = repoGitHubRepository.requestRepoInfo( user.getLogin(), repoName, accessToken );

	    try {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode root = mapper.readTree( response );

		List<RepoGitHub> repoGitHubs = new ArrayList<>();
		repoGitHubs.add( new RepoGitHub( root, user ) );

		return repoGitHubs;
	    } catch ( Exception ex ) {
		ex.printStackTrace();
	    }
	}


	return null;
    }
}
