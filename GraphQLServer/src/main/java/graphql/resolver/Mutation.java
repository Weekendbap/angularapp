package graphql.resolver;

import java.util.List;

import graphql.model.Issue;
import graphql.schema.DataFetchingEnvironment;
import graphql.service.IssueService;
import graphql.servlet.GraphQLContext;

import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;

/**
 * Mutation
 * To create, update something to data source
 */
@Component
public class Mutation implements GraphQLMutationResolver {
    IssueService issueService;

    /**
     * Constructor
     */
    public Mutation() {
	this.issueService = new IssueService();
    }

    /**
     * Create a issue
     *
     * @param nameRepo
     * @param title
     * @param body
     * @param labels
     * @param env
     * @return
     */
    public Issue createIssue( String nameRepo, String title, String body, List<String> labels, DataFetchingEnvironment env ) {
	String accessToken = ((GraphQLContext) env.getContext()).getRequest().get().getHeader( "Authorization" );
	return issueService.createIssue( nameRepo, title, body, labels, accessToken );
    }

    /**
     * Patch a issue
     * @param number
     * @param nameRepo
     * @param title
     * @param body
     * @param labels
     * @param env
     * @return
     */
    public Issue patchIssue( Integer number, String nameRepo, String title, String body, List<String> labels, DataFetchingEnvironment env ) {
	String accessToken = ((GraphQLContext) env.getContext()).getRequest().get().getHeader( "Authorization" );
	return issueService.patchIssue( number, nameRepo, title, body, labels, accessToken );
    }
}