package graphql.resolver;

import java.util.List;

import graphql.model.RepoGitHub;
import graphql.model.User;
import graphql.schema.DataFetchingEnvironment;
import graphql.service.RepoGitHubService;
import graphql.servlet.GraphQLContext;

import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;

/**
 * User resolver
 */
@Component
public class UserResolver implements GraphQLResolver<User> {
    private RepoGitHubService repoGitHubService;

    /**
     * Constructor
     */
    public UserResolver() {
	this.repoGitHubService = new RepoGitHubService();
    }

    /**
     * Get a repository/repositories info
     * @param user
     * @param repoName
     * @param env
     * @return
     */
    public List<RepoGitHub> getRepos( User user, String repoName, DataFetchingEnvironment env ) {
	String accessToken = ((GraphQLContext) env.getContext()).getRequest().get().getHeader( "Authorization" );
	//String token = "token d41784e577d8ed4f8dc79c3bba2578f81d6ed4cd";
	return repoGitHubService.getRepos( user, repoName, accessToken );
    }
}