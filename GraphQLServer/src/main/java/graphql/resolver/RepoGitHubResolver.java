package graphql.resolver;

import java.util.List;

import graphql.model.Issue;
import graphql.model.LabelStatistic;
import graphql.model.RepoGitHub;
import graphql.schema.DataFetchingEnvironment;
import graphql.service.IssueService;
import graphql.servlet.GraphQLContext;

import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLResolver;

/**
 * Repository Github resolver
 */
@Component
public class RepoGitHubResolver implements GraphQLResolver<RepoGitHub> {
    private IssueService issueService;

    /**
     * Constructor
     */
    public RepoGitHubResolver() {
	this.issueService = new IssueService();
    }

    /**
     * Get issues info
     *
     * @param repo
     * @return
     */
    public List<Issue> getIssues( RepoGitHub repo, String state, DataFetchingEnvironment env ) {
	String accessToken = ((GraphQLContext) env.getContext()).getRequest().get().getHeader( "Authorization" );
	return issueService.getIssuesInfo( repo.getOwner().getLogin(), repo.getName(), state, accessToken );
    }

    public LabelStatistic getLabelStatistic( RepoGitHub repo, DataFetchingEnvironment env ) {
	String accessToken = ((GraphQLContext) env.getContext()).getRequest().get().getHeader( "Authorization" );
	return issueService.countLabelsForIssue( repo.getOwner().getLogin(), repo.getName(), accessToken );
    }
}
