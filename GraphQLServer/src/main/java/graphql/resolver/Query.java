package graphql.resolver;

import graphql.model.User;
import graphql.schema.DataFetchingEnvironment;
import graphql.service.UserService;
import graphql.servlet.GraphQLContext;

import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.ParseException;
/**
 * Query
 * To query something from data source
 */
@Component
public class Query implements GraphQLQueryResolver {
    private UserService userService;

    /**
     * Constructor
     */
    public Query() {
	this.userService = new UserService();
    }

    /**
     * Get a user info
     *
     * @param env
     * @return
     */
    public User viewer( DataFetchingEnvironment env ) {
	String accessToken = ((GraphQLContext) env.getContext()).getRequest().get().getHeader( "Authorization" );
	//String token = "token d41784e577d8ed4f8dc79c3bba2578f81d6ed4cd";
    System.out.println("////////////////////////////////////////////////////");
    try {
        SimpleDateFormat formatter = new SimpleDateFormat("mm/dd/yyyy, HH:mm:ss");
        String d1 = ((GraphQLContext) env.getContext()).getRequest().get().getHeader( "Time" );
        Date currentDate = new Date();
        long timeDiff = currentDate.getTime() - formatter.parse(d1).getTime();
        System.out.println("Date Time of Client: " + d1);
        System.out.println("Date time of ServerGraphQl: " + currentDate);
        System.out.println("Time Diff from Client to serverGraphQl: " + timeDiff / 1000 % 60 + "s");
        timeDiff = 0;
        // long diffSeconds = diff / 1000 % 60;  
        // long diffMinutes = diff / (60 * 1000) % 60; 
        // long diffHours = diff / (60 * 60 * 1000);
        } catch (ParseException e) {
            e.printStackTrace();
        }
	return userService.getUserInfo( accessToken );
    }
}
