package graphql.controller;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping( "/" )
public class TokenController {

    @RequestMapping( value = "/token", method = RequestMethod.GET )
    @ResponseBody
    public Map<String, String> getToken( @RequestParam String code ) {
	String url = "https://github.com/login/oauth/access_token";
	try {
	    URL obj = new URL( url );
	    HttpsURLConnection conn = (HttpsURLConnection) obj.openConnection();

	    conn.setRequestMethod( "POST" );
	    conn.setRequestProperty( "User-Agent", "Mozilla/5.0" );
	    conn.setRequestProperty( "Accept-Language", "en-US,en;q=0.5" );

	    String urlParameters = "client_id=60915e68fa06a37e81f8&client_secret=8d818fd3b263ca550df66dbedee93dc2ee0ec795&code=" + code;

	    // Send post request
	    conn.setDoOutput( true );
	    DataOutputStream wr = new DataOutputStream( conn.getOutputStream() );
	    wr.writeBytes( urlParameters );
	    wr.flush();
	    wr.close();

	    int responseCode = conn.getResponseCode();
	    System.out.println( "\nSending 'POST' request to URL : " + url );
	    System.out.println( "Post parameters : " + urlParameters );
	    System.out.println( "Response Code : " + responseCode );

	    BufferedReader in = new BufferedReader(
			    new InputStreamReader( conn.getInputStream() ) );
	    String inputLine;
	    StringBuffer response = new StringBuffer();

	    while ( (inputLine = in.readLine()) != null ) {
		response.append( inputLine );
	    }
	    in.close();

	    //print result
	    System.out.println( response.toString() );

	    String[] splitStr = response.toString().substring( 0, response.indexOf( "&" ) ).split( "=" );

	    // If response is valid
	    if ( splitStr.length > 1 ) {
		Map<String, String> res = new HashMap<String, String>();
		res.put( splitStr[0], splitStr[1] );
		return res;
	    }

	} catch ( Exception e ) {
	    e.printStackTrace();
	}

	return null;
    }

}
