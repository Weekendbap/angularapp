import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { HomepageComponent } from './core_components/homepage/homepage.component';
import { ChartComponent } from './core_components/chart/chart.component';
import { LoginComponent } from './core_components/login/login.component';
import { RepositoriesComponent } from './core_components/repositories/repositories.component';
import { GetTokenComponent } from './core_components/get-token/get-token.component';
import { ReposComponent } from './core_components/repos/repos.component';
import { IssuesChartComponent } from './core_components/issues-chart/issues-chart.component';
import { IssuesDetailComponent } from './core_components/issues-detail/issues-detail.component';
import { IssuesComponent } from './core_components/issues/issues.component';

import { LayoutComponent } from './shared_components/layout/layout.component';
import { GitLayoutComponent } from './shared_components/git-layout/git-layout.component';

import { AuthGuard } from './shared/guards/auth.guard';
import { PieChartComponent } from './core_components/tram_chart/pie-chart/pie-chart.component';
import { ColumnChartComponent } from './core_components/tram_chart/column-chart/column-chart.component';
import {NhuChartComponent} from "./core_components/nhu_chart/nhu-chart.component";

import { TableRpComponent } from './core_components/tram_chart/table-rp/table-rp.component';
import { TramLayoutComponent } from './core_components/tram_chart/tram-layout/tram-layout.component';
import { PieChartLabelComponent } from './core_components/tram_chart/pie-chart-label/pie-chart-label.component';
import { LineChartComponent } from './core_components/tram_chart/line-chart/line-chart.component';
import { GoogleChartComponent } from './core_components/tram_chart/google-chart/google-chart.component';

const routes: Routes = [
                        { path: '',
                          component: LayoutComponent,
                          children: [
                          { path: '', redirectTo: '/homepage', pathMatch: 'full'},
                          { path: 'repositories', component: RepositoriesComponent, canActivate: [AuthGuard] },
                          { path: 'homepage', component: HomepageComponent, canActivate: [AuthGuard] },
                          { path: 'chart', component: ChartComponent, canActivate: [AuthGuard] },
                            { path: 'nhu', component: NhuChartComponent, canActivate: [AuthGuard] },
                          { path: 'token', component: GetTokenComponent },
                          { path: 'tram-pie-chart', component: PieChartComponent },
                          { path: 'tram-column-chart', component: ColumnChartComponent },
                          { path: 'tram-tbrp', component: TableRpComponent },
                          { path: 'tram-layout', component: TramLayoutComponent },
                          { path: 'tram-pie-chart-label', component: PieChartLabelComponent },
                          { path: 'tram-line-chart', component: LineChartComponent },
                          { path: 'tram-gg-chart', component: GoogleChartComponent }
                        ]},
                        { path: '',
                          component: GitLayoutComponent,
                          children: [
                          { path: 'repos', component: ReposComponent },
                          { path: 'issues', component: IssuesComponent },
                          { path: 'graph', component: IssuesChartComponent },
                          { path: 'detail', component: IssuesDetailComponent }
                        ]},
                        { path: 'login', component: LoginComponent },
                        { path: '**', redirectTo: '' }];

@NgModule({
  imports: [BrowserModule, RouterModule.forRoot(routes)],
  exports: [
    RouterModule
  ]
})
export class AppRouters {}
