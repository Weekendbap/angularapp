import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GitLayoutComponent } from './git-layout.component';

describe('GitLayoutComponent', () => {
  let component: GitLayoutComponent;
  let fixture: ComponentFixture<GitLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GitLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GitLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
