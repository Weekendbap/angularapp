import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-git-layout',
  templateUrl: './git-layout.component.html',
  styleUrls: [
    './git-layout.component.css',
    '../../../assets/css/lib-github.css',
    '../../../assets/css/lib-github2.css']
})
export class GitLayoutComponent implements OnInit {
  repos: any;
  loading: boolean = true;
  repo_name: string;
  issue_name: string;

  constructor(
    private app_service: ApiService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    // this.getRepos();
    this.getRepoName();
    this.getIssueName();
  }

  getRepos() {
    this.app_service.get_list_repo().subscribe((data: any) => {
      this.repos = data.data.viewer.repos;
      this.loading = false;
      console.log(this.repos);
     });
  }

  showRepo(name: string) {
    this.router.navigate(['/issues'], { queryParams: { repo_name: name }});
  }

  getRepoName() {
    this.route.queryParams.subscribe(params => {
      this.repo_name = params.repo_name;
    });
  }

  getIssueName() {
    this.route.queryParams.subscribe(params => {
      this.issue_name = params.issue_name;
    });
  }
}
