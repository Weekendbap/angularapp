import { Component, OnInit } from '@angular/core';

import { AuthenticationService } from '../../shared/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent {

  constructor( 
  	private authenticationService: AuthenticationService,
  	private router: Router ) { }

  logout() {
    if (localStorage.getItem('token')) {
      this.authenticationService.logoutGithub();
    }
    else {
      this.authenticationService.logout();
      this.router.navigate(["login"]);
    }
  }
}
