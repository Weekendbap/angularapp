import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../shared/material/material.module';
import { RouterModule } from '@angular/router';

import { NavbarComponent } from './navbar/navbar.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { GitLayoutComponent } from './git-layout/git-layout.component';

@NgModule({
  declarations: [NavbarComponent, ToolbarComponent, GitLayoutComponent],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule
  ],
  exports: [NavbarComponent, ToolbarComponent]
})
export class SharedModule { }