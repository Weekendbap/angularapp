import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class ApiService {
  apiURL: string = 'http://localhost:3000';
  baseURL: string = 'https://github.com/login/oauth/access_token';

  private httpOptions = {
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json; charset=utf-8')
          // .set('Origin','http://localhost')
          // .set('Access-Control-Request-Method', 'POST, GET')
          // .set('Access-Control-Request-Headers', 'content-type')
        };

  constructor(private http: HttpClient) { }

  list_animal() {
    return this.http.get(`${this.apiURL}/animal+ac`).pipe(map(data => {
      // return data.json();
      return data;
    }))
  }

  get_animal_by_id(animal_id) {
    return this.http.get(`${this.apiURL}/animal/` + animal_id).pipe(map(data => {
      return data;
    }))
  }

  test() {
    const query = `{ "query": "{viewer {company, avatar_url, login}}" }`;
    return this.http.post("http://localhost:8080/graphql",query, this.httpOptions).pipe(map(data => {
      return data;
    }))
  }

  get_repos() {
    const query = `{"query": 
                      "{ viewer {\
                          repos {\
                            name,\
                            created_at,\
                            issues(state:\\"all\\") {\
                              number,\
                              state,\
                              number,\
                              title,\
                              body,\
                              created_at,\
                              closed_at,\
                              labels\
                            }\
                          }\
                        }\
                      }"\
                    }`;
    return this.http.post("http://localhost:8080/graphql",query, this.httpOptions).pipe(map(data => {
      return data;
    }))
  }

  get_label() {
    const query = `{"query": 
                      "{ viewer {\
                          repos {\
                            name,\
                            created_at,\
                            labelStatistic {\
                              bug,\
                              duplicate,\
                              enhancement,\
                              goodFirstIssue,\
                              helpWanted,\
                              invalid,\
                              newFeature,\
                              question,\
                              wontfix\
                            }\
                          }\
                        }\
                      }"\
                    }`;
    return this.http.post("http://localhost:8080/graphql",query, this.httpOptions).pipe(map(data => {
      return data;
    }))
  }

  get_repo_by_name(repo_name: string) {
    const query = `{"query": 
                      "{ viewer {\
                          repos(repoName:\\"${repo_name}\\") {\
                            name,\
                            created_at,\
                            issues(state:\\"all\\") {\
                              number,\
                              state,\
                              title,\
                              body,\
                              created_at,\
                              closed_at,\
                              labels\
                            }\
                          }\
                        }\
                      }"\
                    }`;
    return this.http.post("http://localhost:8080/graphql",query, this.httpOptions).pipe(map(data => {
      return data;
    }))
  }

  get_list_repo() {
    const query = `{"query": 
                      "{ viewer {\
                          repos {\
                            name,\
                            created_at,\
                            issues(state:\\"all\\") {\
                              number\
                            }\
                          }\
                        }\
                      }"\
                    }`;
    return this.http.post("http://localhost:8080/graphql",query, this.httpOptions).pipe(map(data => {
      console.log("Time of Client: " + new Date().toLocaleString())
      return data;
    }))
  }

  get_label_by_repo_name(repo_name: string) {
    const query = `{"query": 
                      "{ viewer {\
                          repos(repoName:\\"${repo_name}\\") {\
                            name,\
                            created_at,\
                            labelStatistic {\
                              bug,\
                              duplicate,\
                              enhancement,\
                              goodFirstIssue,\
                              helpWanted,\
                              invalid,\
                              newFeature,\
                              question,\
                              wontfix\
                            }\
                          }\
                        }\
                      }"\
                    }`;
    return this.http.post("http://localhost:8080/graphql",query, this.httpOptions).pipe(map(data => {
      return data;
    }))
  }

  get_issue_by_repo_name(repo_name: string) {
    const query = `{"query": 
                      "{ viewer {\
                          repos(repoName:\\"${repo_name}\\") {\
                            name,\
                            created_at,\
                            issues(state:\\"all\\") {\
                              number,\
                              state,\
                              title,\
                              body,\
                              created_at,\
                              closed_at,\
                              labels\
                            },\
                            labelStatistic {\
                              bug,\
                              duplicate,\
                              enhancement,\
                              goodFirstIssue,\
                              helpWanted,\
                              invalid,\
                              newFeature,\
                              question,\
                              wontfix\
                            }\
                          }\
                        }\
                      }"\
                    }`;
    return this.http.post("http://localhost:8080/graphql",query, this.httpOptions).pipe(map(data => {
      return data;
    }))
  }

  to_array(json, property) {
    return Object.keys(json).map(function(key){ return json[key][property] })
  }
}
