import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from '../../../environments/environment';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  // url login for github
  loginURL: string = 'https://github.com/login/oauth/authorize?client_id=60915e68fa06a37e81f8&scope=user%20notifications%20repo';
  logoutGitURL: string = 'https://github.com/logout';
  private base_URL= environment.base_URL;
  private API_URL= environment.API_URL;

  constructor(private http: HttpClient) { }

  login(username: string, password: string) {
    return this.http.post<any>(`${this.base_URL}/users/authenticate`, { username, password })
      .pipe(map(user => {
        // login successful if there's a user in the response
        if (user) {
          // store user details and basic auth credentials in local storage 
          // to keep user logged in between page refreshes
          user.authdata = window.btoa(username + ':' + password);
          localStorage.setItem('currentUser', JSON.stringify(user));
        }

        return user;
      }));
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    localStorage.removeItem('token');
  }

  logoutGithub () {
    window.open(this.logoutGitURL, '_self');
    localStorage.removeItem('currentUser');
    localStorage.removeItem('token');
  }

  getCode() {
    window.open(this.loginURL, '_self');
  }

  getAccessToken(code: string) {
    return this.http.get(`${this.API_URL}/token?code=` + code).pipe(map((data: any) => {
      return data;
    }));
  }
}