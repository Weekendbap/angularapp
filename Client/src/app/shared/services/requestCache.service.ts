import { Injectable } from "@angular/core";
import { HttpResponse } from "@angular/common/http";

@Injectable({ providedIn: 'root' })
export class RequestCacheService {
  private cache = new Map<string, [Date, HttpResponse<any>]>();

  get(key): HttpResponse<any> {
    var tuple = this.cache.get(key);
    console.log(key)
    console.log(tuple)
    console.log(this.cache)
    if (!tuple) return null;

    const expires = tuple[0];
    const httpResponse = tuple[1];
    console.log("caching");

    // Don't observe expired keys
    const now = new Date();
    if (expires && expires.getTime() < now.getTime()) {
      this.cache.delete(key);
      console.log("expired");
      console.log(expires.toLocaleString())
      console.log(now.toLocaleString())
      return null;
    }
    console.log("not expire yet");
    return httpResponse;
  }

  set(key, value, ttl = null) {
    if (ttl) {
      const expires = new Date();
      expires.setSeconds(expires.getSeconds() + ttl);
      this.cache.set(key, [expires, value]);
    } else {
      this.cache.set(key, [null, value]);
    }
  }
}