import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatMenuModule, MatSidenavModule,
         MatGridListModule, MatCardModule, MatToolbarModule,
         MatIconModule, MatListModule, MatFormFieldModule,
         MatInputModule, MatTableModule, MatPaginatorModule,
         MatProgressSpinnerModule, MatSelectModule, MatSortModule,
         MatSlideToggleModule, MatCheckboxModule, MatRadioModule } from '@angular/material';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatButtonModule,
    MatMenuModule,
    MatSidenavModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MatGridListModule,
    MatCardModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatInputModule,
    MatFormFieldModule,
    MatTableModule,
    MatPaginatorModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatSlideToggleModule,
    MatCheckboxModule,
    MatRadioModule
  ],
  exports: [
  	MatButtonModule,
  	MatMenuModule,
    MatSidenavModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MatGridListModule,
    MatCardModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatInputModule,
    MatFormFieldModule,
    MatTableModule,
    MatPaginatorModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatSlideToggleModule,
    MatCheckboxModule,
    MatRadioModule
  ]
})
export class MaterialModule { }
