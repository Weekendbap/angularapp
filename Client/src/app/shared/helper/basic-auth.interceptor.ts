import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class BasicAuthInterceptor implements HttpInterceptor {
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add authorization header with basic auth credentials if available
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    let token = localStorage.getItem('token');

    if (currentUser && currentUser.authdata) {
      request = request.clone({
          setHeaders: { 
              Authorization: `Basic ${currentUser.authdata}`
          }
      });
    }
    if (token) {
      request = request.clone({
          setHeaders: { 
              Authorization: `token ${token}`,
              Time: new Date().toLocaleString()
          }
      });
    }

    return next.handle(request);
  }
}