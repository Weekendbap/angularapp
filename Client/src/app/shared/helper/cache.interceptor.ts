import { Injectable } from "@angular/core";
import { HttpEvent, HttpResponse } from "@angular/common/http";
import { RequestCacheService } from "../services/requestCache.service";

import { Observable } from 'rxjs';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';

import {
  HttpRequest,
  HttpHandler,
  HttpInterceptor
} from "@angular/common/http";

const TTL = 60;

@Injectable()
export class CacheInterceptor implements HttpInterceptor {
  constructor(private cache: RequestCacheService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    const cachedResponse = this.cache.get(req.body);
    console.log(cachedResponse)
    return cachedResponse ? Observable.of(cachedResponse) : this.sendRequest(req, next);
  }

  sendRequest(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(req).do(event => {
      if (event instanceof HttpResponse) {
        console.log("sending request" );
        console.log(req);
        this.cache.set(req.body, event, TTL);
        console.log(this.cache)
      }
    });
  }
}