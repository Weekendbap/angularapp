import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts';
import { MaterialModule } from '../shared/material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HomepageComponent } from './homepage/homepage.component';
import { ChartComponent } from './chart/chart.component';
import { LoginComponent } from './login/login.component';
import { NhuChartComponent } from './nhu_chart/nhu-chart.component';
import { ReposComponent } from './repos/repos.component';
import { IssuesChartComponent } from './issues-chart/issues-chart.component';

import { ApiService } from '../shared/services/api.service';
import { RepositoriesComponent } from './repositories/repositories.component';
import { GetTokenComponent } from './get-token/get-token.component';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BasicAuthInterceptor } from '../shared/helper/basic-auth.interceptor';
import { ErrorInterceptor } from '../shared/helper/error.interceptor';
import { fakeBackendProvider } from '../shared/helper/fake-backend';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { RouterModule } from '@angular/router';
import { PieChartComponent } from './tram_chart/pie-chart/pie-chart.component';
import { ColumnChartComponent } from './tram_chart/column-chart/column-chart.component';

import { IssuesDetailComponent } from './issues-detail/issues-detail.component';
import { TableRpComponent } from './tram_chart/table-rp/table-rp.component';
import { IssuesComponent } from './issues/issues.component';
import { TramLayoutComponent } from './tram_chart/tram-layout/tram-layout.component';
import { PieChartLabelComponent } from './tram_chart/pie-chart-label/pie-chart-label.component';
import { LineChartComponent } from './tram_chart/line-chart/line-chart.component';
import { GoogleChartComponent } from './tram_chart/google-chart/google-chart.component';
import { GoogleChartsModule } from 'angular-google-charts';

import { CacheInterceptor } from '../shared/helper/cache.interceptor';

@NgModule({
  declarations: [HomepageComponent,ChartComponent, LoginComponent, RepositoriesComponent, GetTokenComponent, ReposComponent, IssuesChartComponent, IssuesDetailComponent, PieChartComponent, ColumnChartComponent, IssuesComponent, NhuChartComponent, TableRpComponent, TramLayoutComponent, PieChartLabelComponent, LineChartComponent, GoogleChartComponent],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ChartsModule,
    ReactiveFormsModule,
    NgxChartsModule,
    RouterModule,
    GoogleChartsModule
  ],
  providers: [ ApiService,
             { provide: HTTP_INTERCEPTORS, useClass: BasicAuthInterceptor, multi: true },
             { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
             { provide: HTTP_INTERCEPTORS, useClass: CacheInterceptor, multi: true },

             // provider used to create fake backend
             fakeBackendProvider
             ],
  exports: [HomepageComponent, ChartComponent, LoginComponent,IssuesComponent, NhuChartComponent]
})
export class CoreModule { }
