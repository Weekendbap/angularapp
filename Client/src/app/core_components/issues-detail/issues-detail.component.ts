import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-issues-detail',
  templateUrl: './issues-detail.component.html',
  styleUrls: [
    './issues-detail.component.css',
    '../../../assets/css/lib-github.css',
    '../../../assets/css/lib-github2.css']
})
export class IssuesDetailComponent implements OnInit {
  repo_name: string = "";
  issue_name: string = "";

  constructor(
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
   this.getIssueNameandRepoName();
  }

  getIssueNameandRepoName() {
    this.route.queryParams.subscribe(params => {
      this.repo_name = params.repo_name;
      this.issue_name = params.issue_name;
    });
  }
}
