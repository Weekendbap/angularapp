import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-issues',
  templateUrl: './issues.component.html',
  styleUrls: ['./issues.component.css',
    '../repos/repos.component.css',
    '../../../assets/css/lib-github.css',
    '../../../assets/css/lib-github2.css']
})
export class IssuesComponent implements OnInit {
  displayedColumns: string[] = ['No', 'name', 'created_at', 'option'];
  dataSource;
  repo_name: string;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(
    private route: ActivatedRoute,
    private app_service: ApiService,
    private router: Router) { }

  ngOnInit() {
    this.getRepoName();
  }

  getIssues() {
    this.app_service.get_repos().subscribe((data: any) => { 
      console.log(data.data.viewer),
      // this.respo_name = data.data.viewer.repos[7].name;
      data.data.viewer.repos.forEach(repo => {
        if(repo.issues.length > 0) {
          this.dataSource = repo.issues;
          console.log(this.dataSource);
        }
      });
     });
  }

  getRepoByName(repo_name: string) {
    this.app_service.get_repo_by_name(repo_name).subscribe((data: any) => {
      console.log(data);
      if (data.data.viewer.repos != null) {
        this.dataSource = new MatTableDataSource(data.data.viewer.repos[0].issues);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      }
     });
  }

  getRepoName() {
    this.route.queryParams.subscribe(params => {
      this.repo_name = params.repo_name;
      this.getRepoByName(this.repo_name);
    });
  }

  showIssueDetail(issue_name: string) {
    this.router.navigate(['/detail'], { queryParams: { repo_name: this.repo_name, issue_name: issue_name }});
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
