import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import countBy from 'lodash/countBy';
import {format} from 'date-fns';

@Component({
  selector: 'nhu-chart',
  templateUrl: './nhu-chart.component.html',
  styleUrls: [
    './nhu-chart.component.css']
})
export class NhuChartComponent implements OnInit {
  repos;
  lineData;
  barData;
  pieData;
  pieDataLabel;
  timeFilter = 'date';
  repoFilter;
  repoNames;

  constructor(private app_service: ApiService) { }

  ngOnInit() {
    this.getIssues();
  }

  getIssues() {
    this.app_service.get_repos().subscribe((data: any) => {
      this.repos = data.data.viewer.repos;
      this.repoNames = this.repos.map(repo => repo.name);
      this.repoFilter = this.repoNames[0];
      this.transformDataForLineChart();
      this.transformDataForBarChart();
      this.transformDataForPieChart();
    });
    this.app_service.get_label().subscribe((data: any) => {
      this.repos = data.data.viewer.repos;
      this.repoNames = this.repos.map(repo => repo.name);
      this.repoFilter = this.repoNames[0];
      this.transformDataLabelForPieChart();
    });
  }

  transformDataForLineChart() {
    let transformedData = this.repos.map(repo => ({
      ...repo,
      issues: repo.issues.map(i => ({...i, created_at: this.getDate(i.created_at)}))
    }));
    this.lineData = transformedData.map(repo => {
      const issueCountByDate = countBy(repo.issues, (issue) => issue.created_at);
      return {
        name: repo.name,
        series: Object.keys(issueCountByDate).map(date => ({name: date, value: issueCountByDate[date]}))
      };
    });
  }

  transformDataForBarChart() {
    this.barData = this.repos.map(repo => {
      const issueCountByState = countBy(repo.issues, (issue) => issue.state);
      return {
        name: repo.name,
        series: Object.keys(issueCountByState).map(state => ({name: state, value: issueCountByState[state]}))
      };
    });
  }

  transformDataForPieChart() {
    const selectedRepo = this.repos.find(repo => repo.name === this.repoFilter);
    const issueCountByState = countBy(selectedRepo.issues, (issue) => issue.state);
    this.pieData = Object.keys(issueCountByState).map(state => ({name: state, value: issueCountByState[state]}));
  }

  transformDataLabelForPieChart() {
    const selectedRepo = this.repos.find(repo => repo.name === this.repoFilter);
    const {labelStatistic} = selectedRepo;
    this.pieDataLabel = Object.keys(labelStatistic).map(l => ({ name: l, value: labelStatistic[l]}));
  }

  getDate = (utc) => {
    let dateFormat;
    switch (this.timeFilter) {
      case 'month':
            dateFormat = 'MM/YYYY';
      break;
      case 'year':
            dateFormat = 'YYYY';
      break;
      default: dateFormat = 'DD/MM/YYYY';
    }
    return format(utc, dateFormat);
  }
}
