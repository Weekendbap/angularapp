import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { ReposComponent } from '../repos/repos.component';

import { ActivatedRoute, Router } from '@angular/router';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-issues-chart',
  templateUrl: './issues-chart.component.html',
  styleUrls: ['./issues-chart.component.css']
})
export class IssuesChartComponent implements OnInit {
  repo_name: string;
  data: any = new Object();
  repos: any;
  type_chart: any;
  type_charts: any = {
    1: {
      'name': 'reposPieChart',
      'methodDraw': 'drawReposPieChart',
      'methodGetData': 'getRepos'
    },
    2: {
      'name': 'issuePieChart',
      'methodDraw': 'drawIssuePieChart',
      'methodGetData': 'getLabelStaticticByReposName',
      'params': 'repo_name'//params for methodGetData
    },
    3: {
      'name': 'issueColumnChart',
      'methodDraw': 'drawIssueColumnChart',
      'methodGetData': 'getAllLabelByReposName',
      'params': 'repo_name'//params for methodGetData
    }
  };
  // @Input() repos_component;

  constructor(
    private app_service: ApiService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.getRepoName();
    // this.getRepos();
    this.setTypeChart();
    // this.getRepos();
  }

  getLabel(repo_name) {
    this.app_service.get_label_by_repo_name(repo_name).subscribe((data: any) => { 
      if (data.data.viewer.repos != null) {
        // this.respo_name = data.data.viewer.repos[7].name;
        this.data.labels = this.get_keys_of(data.data.viewer.repos[0].labelStatistic);
        this.data.series = [3,4,4,5,5,66,7,7,7];
        // this.loadChart(this.data);
      }
     });
  }

  loadChart(data: any[], methodDraw: string) {
    this[methodDraw](data);
  }

  get_keys_of(json) {
    return Object.keys(json).map(function(key){ return key })
  }

  get_values_of(json) {
    return Object.keys(json).map(function(key){ return json[key] })
  }

  showIssuePieChart() {
    this.router.navigate(['/graph'], { queryParams: { chart: 2, repo_name: this.repo_name }});
  }

  showIssueColumnChart() {
    this.router.navigate(['/graph'], { queryParams: { chart: 3, repo_name: this.repo_name }});
  }

  getRepoName() {
    this.route.queryParams.subscribe(params => {
      this.repo_name = params.repo_name;
    });
  }

  getRepos() {
    var array_data: any[] = [];
    this.app_service.get_list_repo().subscribe((data: any) => {
      if (data.data != null) {
        this.repos = data.data.viewer.repos;
        this.repos.forEach(repo => {
          array_data.push({ 
              "name" : repo.name,
              "y"  : repo.issues != null ? repo.issues.length : 0
          });
        });
        console.log(array_data);
        this.loadChart(array_data, this.type_chart.methodDraw);
      }
     });
  }

  getLabelStaticticByReposName(repo_name: string) {// get all label statistic of a repository
    var array_data: any[] = [];
    this.app_service.get_label_by_repo_name(repo_name).subscribe((data: any) => { 
      var json_data = data.data.viewer.repos[0].labelStatistic;
      array_data = Object.keys(json_data).map(function(key){ return { name: key, y: json_data[key] } });
      console.log(array_data);
      this.loadChart(array_data, this.type_chart.methodDraw);
     });
  }

  getAllLabelByReposName(repo_name: string) {//get all label of a issue by date time
    var array_data: any[] = [];
    this.app_service.get_repo_by_name(repo_name).subscribe((response: any) => { 
      console.log(response.data.viewer.repos[0].issues)
      // this.paresToDataChart(response.data.viewer.repos[0].issues);
      this.drawIssueColumnChart();
     });
  }

  setTypeChart() {
    this.route.queryParams.subscribe(params => {
      if (params.chart) {
        this.type_chart = this.type_charts[params.chart];
        if (Object.keys(this.type_chart).includes('params')) {
          this[this.type_chart.methodGetData](this[this.type_chart.params]);
        } else {
          this[this.type_chart.methodGetData]();
        }
      }
    });
  }

  drawReposPieChart(data: any[]) {
    // Build the chart
    Highcharts.chart('container', {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
      },
      title: {
        text: 'Overview issues in per repository'
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: false
          },
          showInLegend: true
        }
      },
      series: [{type: "pie",name: 'Brands', colorByPoint: true, data: data
      }]
    });
  }

  drawIssuePieChart(data: any[]) {
    // Build the chart
    Highcharts.chart('container', {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
      },
      title: {
        text: 'Label of issues in repository'
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: false
          },
          showInLegend: true
        }
      },
      series: [{type: "pie",name: 'Brands', colorByPoint: true, data: data
      }]
    });
  }

  drawIssueColumnChart() {
    Highcharts.chart('container', {
      chart: {
          type: 'column'
      },
      title: {
          text: 'Label of issue in a repository'
      },
      subtitle: {
          text: ''
      },
      xAxis: {
          categories: [
              'Jan',
              'Feb',
              'Mar',
              'Apr',
              'May',
              'Jun',
              'Jul',
              'Aug',
              'Sep',
              'Oct',
              'Nov',
              'Dec'
          ],
          crosshair: true
      },
      yAxis: {
          min: 0,
          title: {
              text: 'Number of label'
          }
      },
      tooltip: {
          headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
          pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
              '<td style="padding:0"><b>{point.y} label</b></td></tr>',
          footerFormat: '</table>',
          shared: true,
          useHTML: true
      },
      plotOptions: {
          column: {
              pointPadding: 0.2,
              borderWidth: 0
          }
      },
      series: [{
            type: 'column',
            name: 'Bug',
            data: [0, 0, 0, 0, 5, 7, 0, 0, 0, 0, 0, 0]

        }, {
            type: 'column',
            name: 'Good first issue',
            data: [0, 1, 0, 0, 1, 3, 0, 0, 0, 0, 0, 0]

        }, {
            type: 'column',
            name: 'Documentation',
            data: [0, 2, 0, 0, 1, 2, 0, 0, 0, 0, 0, 0]

        }, {
            type: 'column',
            name: 'Help wanted',
            data: [0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0]

        }]
  });
  }

  paresToDataChart(issues: any) {
    var array_data: any[] = [];
    var month_array: any[] = [0,0,0,0,0,0,0,0,0,0,0,0];
    issues.forEach(issue => {
      let month: number = issue.created_at.split('-')[1];
      if (issue.labels.length > 0) {
        issue.labels.forEach(label => {
          if (array_data.length > 0) {
            // array_data.find(item => {
            //   if (item.name == label) {
            //     console.log("==label"+label)
            //     // item.data[parseInt(month, 10)] = item.data[parseInt(month, 10)] + 1;  
            //     console.log( item.data)  
            //   } else {
            //     console.log("!=label"+label)
            //     array_data.push({
            //       "name" : label,
            //       "data"  : month_array
            //     });
            //     console.log( item.data)
            //   }
            // });
          } else {
            console.log("new label"+label)
            array_data.push({
              "name" : label,
              "data"  : month_array
            });
          }
        });
      }
    });
  }

  // updateDataByMonth(data: any[], month: number) {
  //   return data[parseInt(month, 10)] = data[parseInt(month, 10)] + 1;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
  // }
}