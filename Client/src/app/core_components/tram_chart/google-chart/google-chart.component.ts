import { Component } from '@angular/core';
import { ApiService } from '../../../shared/services/api.service';

@Component({
  selector: 'app-google-chart',
  templateUrl: './google-chart.component.html',
  styleUrls: ['./google-chart.component.css']
})
export class GoogleChartComponent {

  titleRepos = 'The number of issues in the repository';
  titleIssue = 'The top of issue in the repository';
  
  type = 'PieChart';
  columnNames = ['Browser', 'Percentage'];
  options = {};
  width = 850;
  height = 700;
  dataRepoChart = [];
  
  dataRepoTmp = [];
  dataRepos;
  reposNames;
  repoFilter;
  
  dataIssueChart = [];
  issueFilter;
  dataIssueTmp = [];


  constructor(private app_service: ApiService) {}
  ngOnInit() {
    this.getRepos();
    this.getListRepos();
  }

  getRepos() {
    this.app_service.get_repos().subscribe((data: any) => {
      this.dataRepos = data.data.viewer.repos;
      this.reposNames = this.dataRepos.map(repo => repo.name);
      this.repoFilter = 0;
      // console.log("data repos ",this.dataRepos);

      for (let i=0; i<this.dataRepos.length; i++) 
      {
        this.dataRepoTmp[i] = [this.dataRepos[i].name,this.dataRepos[i].issues.length]
      }

      this.dataRepoChart = this.dataRepoTmp;
      if(this.repoFilter != 0) 
      {
        this.changeRepoChart();
      }

    });
  }

  changeRepoChart() 
  {
    let dataIssueType = [];
    let checkLenIssue:0;
    let dataIssue;
    let dataIssueName;
    let dataIssueValue;

    this.app_service.get_issue_by_repo_name(this.repoFilter).subscribe((data:any) => {
      dataIssue = data.data.viewer.repos;
      console.log("data issue ",dataIssue);      
      
      for (let i=0; i<dataIssue.length; i++) 
      {
        const dataIssueRepo = dataIssue[i].labelStatistic;
        console.log("data issue name ",dataIssueRepo);
       
        dataIssueName = Object.keys(dataIssueRepo);
        dataIssueValue = Object.values(dataIssueRepo);
        
        checkLenIssue = dataIssue[i].issues.length;
        console.log("lenght issue ",checkLenIssue);
      }

      for (let i=0; i<dataIssueValue.length; i++) {
        dataIssueType[i] = [dataIssueName[i], dataIssueValue[i]];
      }

      if (checkLenIssue === 0) {
        alert("Don't have any issue !!!");
        this.dataRepoChart = this.dataRepoTmp;
      }
      else {
        this.dataRepoChart = dataIssueType;
      }    
    });
  }  

  getListRepos() {
    this.app_service.get_repos().subscribe((data: any) => {
      this.dataRepos = data.data.viewer.repos;
      this.reposNames = this.dataRepos.map(repo => repo.name);
      this.issueFilter = 0;

      //get top kind of issue
      let max;
      let count;
      let i;
      let valueRepo = [];
      let issueLen = [];

      for (let i=0; i<this.dataRepos.length; i++) 
      {
        issueLen[i] = this.dataRepos[i].issues.length;
      }
      console.log("this.dataRepos[i].issues.length ",issueLen);

      for(i=0 ; i<this.dataRepos.length; i++) {
        valueRepo[i]=0;
      }

      for(let j=0; j<3; j++) {
        max=0
        count=0;
        for(i=0; i<this.dataRepos.length; i++) {
          if(max < issueLen[i]) {
            max = issueLen[i];
            count = i;
          }
        }
        issueLen[count]=0;
        valueRepo[count]= max;
      }

      for (let i=0; i<this.dataRepos.length; i++) 
      {
        this.dataIssueTmp[i] = [this.dataRepos[i].name,valueRepo[i]]
      }
      
      this.dataIssueChart = this.dataIssueTmp;
      if(this.issueFilter != 0) 
      {
        this.changeIssueChart();
      }

    });
  }

  changeIssueChart() 
  {
    let dataIssueType = [];
    let checkLenIssueType:0;
    let issueTypeName;
    let issueTypeValue;
    let dataIssue;

    this.app_service.get_issue_by_repo_name(this.issueFilter).subscribe((data:any) => {
      dataIssue = data.data.viewer.repos;
      // console.log("data issue ",this.dataIssue);      
      
      for (let i=0; i<dataIssue.length; i++) 
      {
        const dataIssueName = dataIssue[i].labelStatistic;
        // console.log("data issue name ",dataIssueName);
       
        issueTypeName = Object.keys(dataIssueName);
        issueTypeValue = Object.values(dataIssueName);        
        checkLenIssueType = dataIssue[i].issues.length;
        // console.log("lenght issue ",checkLen);
      }

      //get top kind of issue
      let max;
      let count;
      let i;
      let valueType = [];

      for(i=0 ; i<issueTypeValue.length; i++) {
        valueType[i]=0;
      }

      for(let j=0; j<3; j++) {
        max=0
        count=0;
        for(i=0; i<issueTypeValue.length; i++) {
          if(max < issueTypeValue[i]) {
            max = issueTypeValue[i];
            count = i;
          }
        }
        issueTypeValue[count]=0;
        valueType[count]= max;
      }

      for (let i=0; i<issueTypeValue.length; i++) {
        dataIssueType[i] = [issueTypeName[i], valueType[i]];
      }

      if (checkLenIssueType === 0) {
        alert("Don't have any kind of issue !!!");
        this.dataIssueChart = this.dataIssueTmp;     
      }
      else {
        this.dataIssueChart = dataIssueType;
      }   
    });
  }  
}
