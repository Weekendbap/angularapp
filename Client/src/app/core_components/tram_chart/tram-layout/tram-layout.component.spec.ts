import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TramLayoutComponent } from './tram-layout.component';

describe('TramLayoutComponent', () => {
  let component: TramLayoutComponent;
  let fixture: ComponentFixture<TramLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TramLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TramLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
