import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableRpComponent } from './table-rp.component';

describe('TableRpComponent', () => {
  let component: TableRpComponent;
  let fixture: ComponentFixture<TableRpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableRpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableRpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
