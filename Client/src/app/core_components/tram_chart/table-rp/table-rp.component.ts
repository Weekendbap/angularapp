import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../shared/services/api.service';

@Component({
  selector: 'app-table-rp',
  templateUrl: './table-rp.component.html',
  styleUrls: ['./table-rp.component.css']
})


export class TableRpComponent implements OnInit {

  dataRepos;

  constructor(private api_service: ApiService) { }

  ngOnInit() {
    this.getRepos();
  }

  getRepos () {
    this.api_service.get_list_repo().subscribe((data: any) =>
    {
      this.dataRepos = data.data.viewer.repos;
      console.log(this.dataRepos);
    });
  }
  displayedColumns: string [] = ['name', 'issues', "created_at"];
}
