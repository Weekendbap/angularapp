import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TramChartComponent } from './pie-chart.component';

describe('TramChartComponent', () => {
  let component: TramChartComponent;
  let fixture: ComponentFixture<TramChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TramChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TramChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
