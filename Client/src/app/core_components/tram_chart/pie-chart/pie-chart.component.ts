import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../shared/services/api.service';

@Component({
  selector: 'app-tram-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.css']
})
export class PieChartComponent implements OnInit {

  chartReady:boolean;
  public pieChartData:number[]=[];
  public pieChartLabels:string[]=[];
  public pieChartType:string = 'pie';

  constructor(private app_service: ApiService) {}
  
  ngOnInit() {
    this.getRepos();
  }

  dataRepos;

  getRepos() {
    this.app_service.get_list_repo().subscribe((data: any) => {
      this.dataRepos = data.data.viewer.repos;
      this.chartReady = true;
      // console.log(this.dataRepos);

      for (let i=0; i < this.dataRepos.length; i++)
      {
        this.pieChartLabels.push(this.dataRepos[i].name);
        this.pieChartData.push(this.dataRepos[i].issues.length);
        // console.log(this.pieChartLabels);
      }
    });
  }

}
