import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../shared/services/api.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-pie-chart-label',
  templateUrl: './pie-chart-label.component.html',
  styleUrls: ['./pie-chart-label.component.css']
})
export class PieChartLabelComponent implements OnInit {

  chartReady:boolean;
  public pieChartData:number[]=[];
  public pieChartLabels:string[]=[];
  public pieChartType:string = 'pie';

  constructor(private app_service: ApiService) {}
  
  ngOnInit() {
    this.getRepos();
    this.getLabel();
  }

  dataRepos;
  dataLabel;
  repoNames;

  getRepos() {
    this.app_service.get_list_repo().subscribe((data: any) => {
      this.dataRepos = data.data.viewer.repos;
      this.chartReady = true;
      // console.log(this.dataRepos);

      for (let i=0; i < this.dataRepos.length; i++)
      {
        this.pieChartLabels.push(this.dataRepos[i].name);
        this.pieChartData.push(this.dataRepos[i].issues.length);
        // console.log(this.pieChartLabels);
      }
    });
  }

  getLabel() {
    this.app_service.get_label().subscribe ((data:any) => {
      this.dataLabel = data.data.viewer.repos;
      this.repoNames = this.dataLabel.map(repo => repo.name);
      console.log(this.repoNames);
    });
  }
}
