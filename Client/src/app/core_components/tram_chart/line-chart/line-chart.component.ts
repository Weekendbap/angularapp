import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../shared/services/api.service';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Label } from 'ng2-charts';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.css']
})
export class LineChartComponent implements OnInit {

  chartReady:boolean = true;// avoid error 'dataset '

  public barChartType:string = 'line';

  public barChartData: ChartDataSets[]= [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Open issue' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Close issue' }
  ];
  public barChartLabels: Label[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  // public barChartLabels: Label[] = [];

  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };

  // dataSource;
  // repoFilter;
  // repoNames;
  // selectedRepo;
  // dataIssue;
  // dataIssueDate;
  // datePipe: any;

  constructor(private app_service: ApiService, datePipe: DatePipe) {
    // this.datePipe = new DatePipe('en-US');
  }

  ngOnInit() {
    // this.getIssues();
  }

  // getIssues() {
  //   this.app_service.get_repos().subscribe((data: any) => {
  //     this.dataSource = data.data.viewer.repos;
  //     console.log(this.dataSource);

  //     this.repoNames = this.dataSource.map(repo => repo.name);
  //     this.repoFilter = this.repoNames[0];
      
  //     this.chanegChart();
  //   });
  // }

  // chanegChart() {
  //   this.selectedRepo = this.dataSource.find(repo => repo.name === this.repoFilter);
    
  //   this.dataIssue = this.selectedRepo.issues;

  //   console.log("select Reppost issue: ", this.dataIssue);

  //   for (let i=0; i<this.dataIssue.length; i++) 
  //   {
  //     let mdate = this.dataIssue[i].created_at;

  //     this.dataIssueDate = this.datePipe.transform(mdate, "dd-MM-yyyy");
  //     console.log("date of issue ",this.dataIssueDate);

  //   }
  // }

}
