import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../shared/services/api.service';
import { AuthenticationService } from '../../shared/services/authentication.service';

@Component({
  selector: 'app-get-token',
  templateUrl: './get-token.component.html',
  styleUrls: ['./get-token.component.css']
})
export class GetTokenComponent implements OnInit {
  code: string = "";
  state: string = "";
  company: string;
  name: string;
  url: string;
  data: any;

  constructor(
    private route: ActivatedRoute,
    private apiService: ApiService,
    private authenticationService: AuthenticationService,
    private router: Router) { }

  ngOnInit() {
    // get code
    this.route.queryParams.subscribe(params => {
      console.log(params);
      this.state = params.state;
      // get token
      if (!localStorage.getItem('token')) {
        this.getAccessToken(params.code);
      }
    });

    //get data
    // this.apiService.test().subscribe((response) => {
    //   this.company = response["data"]["viewer"]["company"],
    //   this.name = response["data"]["viewer"]["login"],
    //   this.url = response["data"]["viewer"]["avatar_url"]
    // });
    // this.apiService.get_repos().subscribe((response) => {
    //  console.log(response)
    // });
  }
  
  getAccessToken(code) {
    this.authenticationService.getAccessToken(code).subscribe(
      (data) => {
        localStorage.setItem('token', data.access_token);
        this.router.navigate(['']);
      },
      (error) => {
        console.log("can not login");
        this.router.navigate(['/login']);
      });
  }

  public test_data() {
    this.apiService.test().subscribe((data) => this.data = data);
  }
}
