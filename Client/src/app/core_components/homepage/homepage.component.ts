import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  chartReady:boolean = false;// avoid error 'dataset '
  pieChartLabels:string[];
  pieChartData:number[];
  pieChartType:string = 'pie';
  pieChartOptions:any = {'backgroundColor': ["#FF6384",
                                             "#4BC0C0",
                                             "#FFCE56",
                                             "#E7E9ED",
                                             "#36A2EB",
                                             "#3333ff",
                                             "#99ff66"],
                          'responsive': true,
                          'maintainAspectRatio': false}

  data_text:any;
  constructor(private app_service: ApiService) {}

  ngOnInit() {
    // this.animal_by_pie_chart()
    // this.get_animal(25)
    this.test_data();
  }

  public animal_by_pie_chart() {
    this.app_service.list_animal()
                    .subscribe((data) => {
                        this.pieChartLabels = this.app_service.to_array(data, 'name')
                        this.pieChartData = this.app_service.to_array(data, 'size')
                        this.chartReady = true
                      });
  }

  public get_animal(animal_id) {
    this.app_service.get_animal_by_id(animal_id).subscribe((data) => {console.log( data)});
  }

  public test_data() {
    this.app_service.test().subscribe((data) => this.data_text = data);
    this.app_service.get_repos().subscribe((data) => console.log(data));
  }

}
