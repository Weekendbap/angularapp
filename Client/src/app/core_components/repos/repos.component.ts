import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';

export interface Repository {
  name: string;
  created_at: Date;
  issues: number;
}

@Component({
  selector: 'app-repos',
  templateUrl: './repos.component.html',
  styleUrls: [
    './repos.component.css',
    '../../../assets/css/lib-github.css',
    '../../../assets/css/lib-github2.css']
})
export class ReposComponent implements OnInit {
  displayedColumns: string[] = ['name', 'created_at', 'issues', 'option'];
  dataSource;
  repo_name: string;
  repos: Repository[];
  chartReady: boolean = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(
    private route: ActivatedRoute,
    private app_service: ApiService,
    private router: Router) { }

  ngOnInit() {
    this.getRepos();
  }

  getRepos() {
    this.app_service.get_list_repo().subscribe((data: any) => {
      if (data.data != null) {
        this.repos = data.data.viewer.repos;
        this.dataSource = new MatTableDataSource(this.repos);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      }
     });
  }

  showIssues(name: string) {
    this.router.navigate(['/issues'], { queryParams: { repo_name: name }});
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  showChart() {
    this.router.navigate(['/graph'], { queryParams: { chart: 1 }});
  }

  showIssuePieChart(name: string) {
    this.router.navigate(['/graph'], { queryParams: { chart: 2, repo_name: name }});
  }

  showIssueColumnChart(name: string) {
    this.router.navigate(['/graph'], { queryParams: { chart: 3, repo_name: name }});
  }
}
