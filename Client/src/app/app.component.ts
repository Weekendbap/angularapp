import { Component, OnInit } from '@angular/core';

import { AuthGuard } from './shared/guards/auth.guard';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  actived: boolean;

  constructor(private auth: AuthGuard) {}

  ngOnInit() {
    // this.actived = this.auth.canActivate();
  }

}
