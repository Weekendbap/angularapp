import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CoreModule } from './core_components/core.module';
import { SharedModule } from './shared_components/shared.module';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';

import { FlexLayoutModule } from '@angular/flex-layout';

import { AppRouters } from './app.routes';

import { RouterModule, Routes } from '@angular/router';
import { GetTokenComponent } from './core_components/get-token/get-token.component';

import { LayoutComponent } from './shared_components/layout/layout.component';
import {DatePipe} from '@angular/common';

@NgModule({
  declarations: [
    AppComponent, LayoutComponent
  ],
  imports: [
    BrowserModule,
    CoreModule,
    SharedModule,
    HttpClientModule,
    AppRouters,
    FlexLayoutModule
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent],
  exports: [AppRouters]
})
export class AppModule { }
